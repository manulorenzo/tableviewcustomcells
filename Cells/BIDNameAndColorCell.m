//
//  BIDNameAndColorCell.m
//  Cells
//
//  Created by manu on 11/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "BIDNameAndColorCell.h"

@implementation BIDNameAndColorCell {
    // Instnace variables
    UILabel *_nameValue;
    UILabel *_colorValue;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
